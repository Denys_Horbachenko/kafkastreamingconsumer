package com.epam.bigdata

import org.apache.spark.sql.types.{IntegerType, LongType, StringType, StructField, StructType}

object StationStatusInfo {

  val schema = new StructType()
    .add("stationId", StringType)
    .add("numBikesAvailable", IntegerType)
    .add("numDocksAvailable", IntegerType)
    .add("lastReported", LongType)
    .add("numEbikesAvailable", IntegerType)
    .add("stationStatus", StringType)
  def getSchema: StructType = {
    schema
  }
}

case class StationStatusInfo(stationId: String, numBikesAvailable: Int,
                             numDocksAvailable: Int, lastReported: Long,
                             numEbikesAvailable: Int, stationStatus: String) {
//  override def toString: String = {
//    s"""{
//       | "LastReported": "$lastReported",
//       | "StationId": "$stationId",
//       | "NumEbikesAvailable": "$numEbikesAvailable",
//       | "NumBikesAvailable": "$numBikesAvailable",
//       | "NumDocksAvailable": "$numDocksAvailable",
//       | "StationStatus": "$stationStatus"
//       |}""".stripMargin
//  }
}