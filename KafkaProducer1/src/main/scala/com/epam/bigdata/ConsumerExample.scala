package com.epam.bigdata

/**
 * @author ${user.name}
 */
import org.apache.spark.sql.SparkSession

import org.apache.spark.sql.functions._

object ConsumerExample extends App {

  val URL = "localhost:9092"
  val FROM_TOPIC = "test2"
  val TO_TOPIC = "test3"

  val spark = SparkSession.builder
    .master("local")
    .appName("ConsumerExample")
    .getOrCreate()

  spark.sparkContext.setLogLevel("ERROR")

  val df = spark.readStream
    .format("kafka")
    .option("kafka.bootstrap.servers", URL)
    .option("subscribe", FROM_TOPIC)
    .load

  import spark.implicits._

  val eventsDF = df.selectExpr("CAST(value AS STRING)")

  val event = eventsDF.select(from_json(col("value"), StationStatusInfo.getSchema)
    .as("data"))
    .select("data.*").as[StationStatusInfo]

    event.writeStream
    .outputMode("append")
    .format("console")
    .option("truncate", "false")
    .start()
    .awaitTermination()

//  event.selectExpr("CAST(stationId AS STRING) AS key", "to_json(struct(*)) AS value")
//    .writeStream
//    .format("kafka")
//    .outputMode("append")
//    .option("kafka.bootstrap.servers", URL)
//    .option("topic", TO_TOPIC)
//    .option("checkpointLocation", "/tmp/checkpoint")
//    .start()
//    .awaitTermination()
}
